Kontalk est une messagerie instantanée sécurisée, vous permettant d'envoyer et de recevoir des textes, images et messages vocaux (d'autres types de fichiers seront ajoutés procahinement) de et vers d'autres utilisateurs Kontalk. Le tout gratuitement.

Vous trouverez l'ensemble des explications relatives aux permissions utilisées par l'application sur [https://github.com/kontalk/androidclient/wiki/Android-client-permissions].

Cette version ne comporte pas les composants Google. Cela signifie que les notifications de type push ne sont pas prises en compte.

(*) Des frais de connexion peuvent s'appliquer selon votre opérateur

