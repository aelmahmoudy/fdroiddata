Categories:System,Development
License:MIT
Author Name:ByteHamster
Author Email:info@bytehamster.com
Web Site:https://gitlab.com/ByteHamster/changelog
Source Code:https://gitlab.com/ByteHamster/changelog/tree/HEAD
Issue Tracker:https://gitlab.com/ByteHamster/changelog/issues

Name:OmniROM Changelog
Auto Name:Changelog

Repo Type:git
Repo:https://gitlab.com/ByteHamster/changelog.git

Build:3.12,31
    commit=v3.12
    subdir=app
    gradle=yes

Build:3.13,32
    commit=v3.13
    subdir=app
    gradle=yes

Build:3.14,34
    commit=v3.14
    subdir=app
    gradle=yes

Build:3.14,35
    commit=v3.14
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:3.14
Current Version Code:35
